using System.Collections.Generic;

/// <summary>
/// Logics Class for the BoxPanel.
/// </summmary>
public class BoxPanelLogicsImpl : GridPanelLogicsImpl, IBoxPanelLogics {
    ///<param name="width">Width of the Panel</param>
    ///<param name="height">Height of the Panel</param>
    ///<param name="rows">Rows of the Panel</param>
    ///<param name="cols">Columns of the Panel</param>
    public BoxPanelLogicsImpl(int width, int height, int rows, int cols) : base(width, height, rows, cols) {
        this.LowerDimensions();
    }

    public void LowerDimensions()
    {
        // Makes width and height slightly lower
        this.SetSize(new KeyValuePair<int, int>(this.GetSize().Key- (this.GetSize().Key/ 10),
                this.GetSize().Value - (this.GetSize().Value/ 10)));
        this.CancelProtrusion();
        this.SetBoxSize(new KeyValuePair<int, int>(this.GetSize().Value/ this.GetnLines().Key,
                this.GetSize().Key / this.GetnLines().Value));
    }
}
