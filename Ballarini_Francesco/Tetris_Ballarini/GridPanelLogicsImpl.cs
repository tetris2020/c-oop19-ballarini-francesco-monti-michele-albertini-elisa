using System;
using System.Collections.Generic;
using System.Drawing;

/// <summary>
/// Logics Class for the GridPanel.
/// </summmary>
public class GridPanelLogicsImpl : IGridPanelLogics 
{
    //X is width, Y is height
    private KeyValuePair<int, int> size;
    //X is rows, Y is cols
    private KeyValuePair<int, int> nLines;
    //X is boxWidth, Y is boxHeight
    private KeyValuePair<int, int> boxsize;
    private HashSet<KeyValuePair<int, int>> piece;
    private Color color;

    ///<param name="width">Width of the Panel</param>
    ///<param name="height">Height of the Panel</param>
    ///<param name="rows">Rows of the Panel</param>
    ///<param name="cols">Columns of the Panel</param>
    public GridPanelLogicsImpl(int width, int height, int rows, int cols) {
        this.size = new KeyValuePair<int,int>(width, height);
        this.nLines = new KeyValuePair<int, int>(rows, cols);
        this.boxsize = new KeyValuePair<int,int>(this.size.Value/this.nLines.Key, this.size.Key/this.nLines.Value);
        this.piece = new HashSet<KeyValuePair<int, int>>();
        this.color = Color.Black;
        this.CancelProtrusion();
    }

    public void PaintLines(Graphics g) {
        //The graphics methods are commented because in this code they aren't meant to be used
        /*
        for (int i = 0; i <= this.nLines.Key; i++) {
            g.drawLine(0, i * this.boxsize.Key, this.size.Key, i * this.boxsize.Value);         
        }

        for (int i = 0; i <= this.nLines.Value; i++) {
            g.drawLine(i * this.boxsize.Value, 0, i * this.boxsize.Key, this.size.Value);
        }
        */
        //Insted, when a graphical change should appear, the output is written in the Console
        Console.Out.WriteLine("Lines drawn");
    }

    public void CancelProtrusion() {
        while (this.size.Key % this.nLines.Value != 0) {
            this.size = new KeyValuePair<int, int>(this.size.Key - 1, this.size.Value);
        }
        while (this.size.Value % this.nLines.Key != 0) {
            this.size = new KeyValuePair<int, int>(this.size.Key, this.size.Value - 1);
        }
    }

    public void FillBlock(int x, int y, Color color, Graphics g) {
        //The graphics methods are commented because in this code they aren't meant to be used
        /*
        g.setColor(color);
        g.fillRect((y) * this.boxsize.Key + 1,(x) * this.boxsize.Value + 1, this.boxsize.Key - 1, this.boxsize.Value - 1);
        */
        //Insted, when a graphical change should appear, the output is written in the Console
        Console.Out.WriteLine("Block " + x + ", " + y + " is filled with color: " + color);
    }

    public void DrawPiece(Graphics g) {
        // It draws the piece
        foreach(KeyValuePair<int, int> coordinate in this.GetPiece())
        {
            this.FillBlock(coordinate.Key, coordinate.Value, this.GetColor(), g);
        }
    }

    public Color GetColor() {
        return this.color;
    }

    public void SetColor(Color color) {
        this.color = color;
    }

    public HashSet<KeyValuePair<int, int>> GetPiece() {
        return this.piece;
    }

    public void SetPiece(HashSet<KeyValuePair<int, int>> piece) {
        this.piece = piece;
    }

    public KeyValuePair<int, int> GetSize() {
        return this.size;
    }

    public void SetSize(KeyValuePair<int, int> size) {
        this.size = size;
    }

    public KeyValuePair<int, int> GetnLines() {
        return this.nLines;
    }

    public void SetBoxSize(KeyValuePair<int, int> boxsize) {
        this.boxsize = boxsize;
    }
}
