﻿using System.Collections.Generic;
using System.Drawing;

/// <summary>
/// Interface for the Logics for the GridPanel.
/// </summmary>
public interface IGridPanelLogics 
{
    /// <summary>
    /// Creates all the lines.
    /// </summmary>
    /// <param name="g">Graphics</param>
    void PaintLines(Graphics g);

    /// <summary>
    /// Slightly changes the width or the height in order to make them divisible by columns and rows respectively.
    /// </summmary>
    void CancelProtrusion();

    /// <summary>
    /// Fills the block (x,y) with the color (color).
    /// </summmary>
    /// <param name="x">x of the block</param>
    /// <param name="y">y of the block</param>
    /// <param name="color">color of the block</param>
    /// <param name="g">Graphics</param>
    void FillBlock(int x, int y, Color color, Graphics g);

    /// <summary>
    /// Draws the saved Piece.
    /// </summmary>
    /// <param name="g">Graphics</param>
    void DrawPiece(Graphics g);

    /// <summary>
    /// Gets the Color of the current piece (null if no piece).
    /// </summmary>
    /// <returns> 
    /// The Color of the current Piece 
    /// </returns>
    Color GetColor();

    /// <summary>
    /// Sets the Color of the current piece (null if no piece).
    /// </summmary>
    /// <param name="color">Color of the current piece</param>
    void SetColor(Color color);

    /// <summary>
    /// Gets the Set of the current piece (empty if no piece).
    /// </summmary>
    /// <returns> 
    /// Set<KeyValuePair<int,int>> of the current Current Piece 
    /// </returns>
    HashSet<KeyValuePair<int, int>> GetPiece();

    /// <summary>
    /// Sets the Set of the current piece (empty if no piece).
    /// </summmary>
    /// <param name="piece">Set<KeyValuePair<int,int>> of the current Current Piece </param>
    void SetPiece(HashSet<KeyValuePair<int, int>> piece);

    /// <summary>
    /// Gets the sizes of the Panel. X is width, Y is height
    /// </summmary>
    /// <returns> 
    /// KeyValuePair<int,int>> of the Size of the Panel
    /// </returns>
    KeyValuePair<int, int> GetSize();

    /// <summary>
    /// Sets the sizes of the Panel. X is width, Y is height.
    /// </summmary>
    /// <param name="size">Size of the Panel</param>
    void SetSize(KeyValuePair<int, int> size);

    /// <summary>
    /// Gets the number of lines of the Panel. X is rows, Y is cols.
    /// </summmary>
    /// <returns> 
    /// KeyValuePair<int,int>> of the number of rows and cols
    /// </returns>
    KeyValuePair<int, int> GetnLines();

    /// <summary>
    /// Sets the sizes of a box in the Panel. X is boxWidth, Y is boxHeight.
    /// </summmary>
    /// <param name="boxSize">width and height of a box</param>
    void SetBoxSize(KeyValuePair<int, int> boxSize);
}
