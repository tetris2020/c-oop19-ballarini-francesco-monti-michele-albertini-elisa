using System.Collections.Generic;
using System.Drawing;

/// <summary>
/// Interface for the Logics for the TetrisPanel.
/// </summmary>
public interface ITetrisPanelLogics : IGridPanelLogics {
    /// <summary>
    /// Draws the Board (the Map containing all the already fallen pieces).
    /// </summmary>
    /// <param name="g">Graphics</param>
    void DrawBoard(Graphics g);

    /// <summary>
    /// Draws the Projection of the falling Piece.
    /// </summmary>
    /// <param name="g">Graphics</param>
    void DrawProjection(Graphics g);

    /// <summary>
    /// Sets the Board (the Map containing all the already fallen pieces).
    /// </summmary>
    /// <param name="board">Map of Pair<Integer,Integer> as keys and Color as values</param>
    void SetBoard(Dictionary<KeyValuePair<int, int>, Color> board);

    /// <summary>
    /// Sets the Set for the Projection of the falling Piece.
    /// </summmary>
    /// <param name="projectionPiece">piece of the projection</param>
    void SetProjection(HashSet<KeyValuePair<int, int>> projectionPiece);

    /// <summary>
    /// Sets the Color for the Projection of the falling Piece.
    /// </summmary>
    /// <param name="projectionColor">Color of the projection</param>
    void SetProjectionColor(Color projectionColor);
}
