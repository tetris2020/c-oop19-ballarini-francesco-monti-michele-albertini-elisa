using System.Collections.Generic;
using System.Drawing;
/// <summary>
/// Logics for the TetrisPanel.
/// </summmary>
public class TetrisPanelLogicsImpl : BoxPanelLogicsImpl, ITetrisPanelLogics {
    private Dictionary<KeyValuePair<int, int>, Color> board;
    private HashSet<KeyValuePair<int, int>> projectionPiece;
    private Color projectionColor;

    ///<param name="width">Width of the Panel</param>
    ///<param name="height">Height of the Panel</param>
    ///<param name="rows">Rows of the Panel</param>
    ///<param name="cols">Columns of the Panel</param>
    public TetrisPanelLogicsImpl(int width, int height, int rows, int cols) : base(width, height, rows, cols)
    {
        this.board = new Dictionary<KeyValuePair<int, int>, Color>();
        this.projectionPiece = new HashSet<KeyValuePair<int, int>>();
        this.projectionColor = Color.Black;
    }

    public void DrawBoard(Graphics g) {
        foreach(KeyValuePair<int, int> coordinate in this.board.Keys) {
            this.FillBlock(coordinate.Key, coordinate.Value, this.board[coordinate], g);
        }
    }

    public void DrawProjection(Graphics g) {
        foreach(KeyValuePair<int, int> coordinate in this.projectionPiece) {
            this.FillBlock(coordinate.Key, coordinate.Value, this.projectionColor, g);
        }
    }

    public void SetBoard(Dictionary<KeyValuePair<int, int>, Color> board) {
        this.board = board;
    }

    public void SetProjection(HashSet<KeyValuePair<int, int>> projectionPiece) {
        this.projectionPiece = projectionPiece;
    }

    public void SetProjectionColor(Color projectionColor) {
        this.projectionColor = projectionColor;
    }
}
