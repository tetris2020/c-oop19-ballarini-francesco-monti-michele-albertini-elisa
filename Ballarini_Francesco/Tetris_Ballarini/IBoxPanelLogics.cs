/// <summary>
/// Interface for the Logics of the BoxPanel.
/// </summmary>
public interface IBoxPanelLogics : IGridPanelLogics {
    /// <summary>
    /// Lowers the dimensions (height and width) of the BoxPanel.
    /// </summmary>
    void LowerDimensions();
}
