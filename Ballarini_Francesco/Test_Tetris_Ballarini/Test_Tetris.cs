﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_Tetris_Ballarini
{
    [TestClass]
    public class Test_Tetris
    {
        private StringWriter stringWriter;

        //For each test, i'll do an action with both the TetrisGamePanelLogicsImpl and the BoxPanelLogicsImpl
        //I'll then test if the Console output for that specific action (which is what the Class would have drawn) is equal to the output I expected
        [TestMethod]
        public void Test_Drawing_Lines_Tetris()
        {
            Graphics g = null;
            ITetrisPanelLogics tetris = new TetrisPanelLogicsImpl(500, 1000, 10, 20);

            stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            tetris.PaintLines(g);

            String s = stringWriter.ToString();
            Assert.AreEqual("Lines drawn\r\n", stringWriter.ToString());
        }

        [TestMethod]
        public void Test_Drawing_Lines_Box()
        {
            Graphics g = null;
            IBoxPanelLogics box = new BoxPanelLogicsImpl(500, 500, 4, 4);

            stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            box.PaintLines(g);

            String s = stringWriter.ToString();
            Assert.AreEqual("Lines drawn\r\n", stringWriter.ToString());
        }

        [TestMethod]
        public void Test_Single_Block_Tetris()
        {
            Graphics g = null;
            ITetrisPanelLogics tetris = new TetrisPanelLogicsImpl(500, 1000, 10, 20);

            stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            tetris.FillBlock(1, 2, Color.Red, g);
            String s = stringWriter.ToString();
            Assert.AreEqual("Block 1, 2 is filled with color: Color [Red]\r\n", stringWriter.ToString());
        }

        [TestMethod]
        public void Test_Single_Block_Box()
        {
            Graphics g = null;
            IBoxPanelLogics box = new BoxPanelLogicsImpl(500, 500, 4, 4);

            stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            box.FillBlock(1, 2, Color.Red, g);
            String s = stringWriter.ToString();
            Assert.AreEqual("Block 1, 2 is filled with color: Color [Red]\r\n", stringWriter.ToString());
        }

        [TestMethod]
        public void Test_Piece_Tetris()
        {
            Graphics g = null;
            ITetrisPanelLogics tetris = new TetrisPanelLogicsImpl(500, 1000, 10, 20);

            stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            HashSet<KeyValuePair<int, int>> piece = new HashSet<KeyValuePair<int, int>>();
            piece.Add(new KeyValuePair<int, int>(1,1));
            piece.Add(new KeyValuePair<int, int>(1,2));
            piece.Add(new KeyValuePair<int, int>(1,3));
            piece.Add(new KeyValuePair<int, int>(1,4));

            tetris.SetPiece(piece);
            tetris.SetColor(Color.Red);
            tetris.DrawPiece(g);

            String s = stringWriter.ToString();
            Assert.AreEqual("Block 1, 1 is filled with color: Color [Red]\r\n" +
                "Block 1, 2 is filled with color: Color [Red]\r\n" +
                "Block 1, 3 is filled with color: Color [Red]\r\n" +
                "Block 1, 4 is filled with color: Color [Red]\r\n", stringWriter.ToString());
        }

        [TestMethod]
        public void Test_Piece_Box()
        {
            Graphics g = null;
            IBoxPanelLogics box = new BoxPanelLogicsImpl(500, 500, 4, 4);

            stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            HashSet<KeyValuePair<int, int>> piece = new HashSet<KeyValuePair<int, int>>();
            piece.Add(new KeyValuePair<int, int>(1, 1));
            piece.Add(new KeyValuePair<int, int>(1, 2));
            piece.Add(new KeyValuePair<int, int>(1, 3));
            piece.Add(new KeyValuePair<int, int>(1, 4));

            box.SetPiece(piece);
            box.SetColor(Color.Red);
            box.DrawPiece(g);

            String s = stringWriter.ToString();
            Assert.AreEqual("Block 1, 1 is filled with color: Color [Red]\r\n" +
                "Block 1, 2 is filled with color: Color [Red]\r\n" +
                "Block 1, 3 is filled with color: Color [Red]\r\n" +
                "Block 1, 4 is filled with color: Color [Red]\r\n", stringWriter.ToString());
        }

        [TestMethod]
        public void Test_Projection()
        {
            Graphics g = null;
            ITetrisPanelLogics tetris = new TetrisPanelLogicsImpl(500, 1000, 10, 20);

            stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            HashSet<KeyValuePair<int, int>> projection = new HashSet<KeyValuePair<int, int>>();
            projection.Add(new KeyValuePair<int, int>(5, 1));
            projection.Add(new KeyValuePair<int, int>(5, 2));
            projection.Add(new KeyValuePair<int, int>(5, 3));
            projection.Add(new KeyValuePair<int, int>(5, 4));

            tetris.SetProjection(projection);
            tetris.SetProjectionColor(Color.White);
            tetris.DrawProjection(g);

            String s = stringWriter.ToString();
            Assert.AreEqual("Block 5, 1 is filled with color: Color [White]\r\n" +
                "Block 5, 2 is filled with color: Color [White]\r\n" +
                "Block 5, 3 is filled with color: Color [White]\r\n" +
                "Block 5, 4 is filled with color: Color [White]\r\n", stringWriter.ToString());
        }

        [TestMethod]
        public void Test_Board()
        {
            Graphics g = null;
            ITetrisPanelLogics tetris = new TetrisPanelLogicsImpl(500, 1000, 10, 20);

            stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            Dictionary<KeyValuePair<int, int>, Color> board = new Dictionary<KeyValuePair<int, int>, Color>();
            board[new KeyValuePair<int, int>(1, 1)] = Color.Green;
            board[new KeyValuePair<int, int>(1, 2)] = Color.Green;
            board[new KeyValuePair<int, int>(2, 2)] = Color.Yellow;
            board[new KeyValuePair<int, int>(3, 3)] = Color.Green;
            board[new KeyValuePair<int, int>(3, 4)] = Color.Yellow;

            tetris.SetBoard(board);
            tetris.DrawBoard(g);

            String s = stringWriter.ToString();
            Assert.AreEqual("Block 1, 1 is filled with color: Color [Green]\r\n" +
                "Block 1, 2 is filled with color: Color [Green]\r\n" +
                "Block 2, 2 is filled with color: Color [Yellow]\r\n" +
                "Block 3, 3 is filled with color: Color [Green]\r\n" +
                "Block 3, 4 is filled with color: Color [Yellow]\r\n", stringWriter.ToString());
        }

    }
}
