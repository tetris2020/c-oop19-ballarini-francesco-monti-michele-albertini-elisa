﻿using System;
using System.Collections.Generic;
using System.Text;

namespace oop19
{
    public class ScoreImpl : IScore
    {

        private int value, pointsToNextLevel;
        private static int levelUp = 150;
        public ScoreImpl()
        {
            this.pointsToNextLevel = levelUp;
        }
        public void AddPoints(int combo)
        {
            int points = 0;
            for (int i = 0; i < combo; i++)
            {
                points += (combo - i) * 10;
            }
            this.value = this.value + points;
            this.pointsToNextLevel = this.pointsToNextLevel - points;
            while (this.pointsToNextLevel <= 0)
            {
                this.pointsToNextLevel = this.pointsToNextLevel + levelUp;
            }
        }

        public int Score
        {
            get { return this.value; }
        }
    }
}
