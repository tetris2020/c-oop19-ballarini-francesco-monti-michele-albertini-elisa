﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace oop19
{
    public interface IBoard
    {
        void CancelLines(HashSet<int> rows);

        Dictionary<KeyValuePair<int, int>, Color> Board
        {
            get;
        }

        void FindRowsCompleted();

        void PlacePiece(KeyValuePair<int, int> coords, Color color);

    }
}
