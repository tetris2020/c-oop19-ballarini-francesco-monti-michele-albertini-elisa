﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace oop19
{
    public class BoardImpl : IBoard
    {
        private Dictionary<KeyValuePair<int, int>, Color> board = new Dictionary<KeyValuePair<int, int>, Color>();
        private static readonly int rowLength = 10;
        private static readonly int colLength = 20;
        public Dictionary<KeyValuePair<int, int>, Color> Board
        {
            get
            {
                System.Console.WriteLine(this.board);
                return this.board;
            }
        }

        public void PlacePiece(KeyValuePair<int, int> coords, Color color)
        {
            this.board.Add(coords, color);
        }

        public void CancelLines(HashSet<int> rows)
        {
            foreach (int row in rows)
            {
                for (int i = 0; i < rowLength; i++)
                {
                    this.board.Remove(new KeyValuePair<int, int>(row, i));
                }
            }
            this.UpdateField(rows);
        }

        public void FindRowsCompleted()
        {
            HashSet<int> completeLines = new HashSet<int>();
            foreach (var i in this.board)
            {
                int count = 0;
                if (!completeLines.Contains(i.Key.Key))
                {
                    foreach (var blocks in this.board)
                    {
                        if (blocks.Key.Key.Equals(i.Key.Key))
                        {
                            count++;
                        }
                    }
                    if (count == rowLength)
                    {
                        completeLines.Add(i.Key.Key);
                    }
                }
            }
            this.CancelLines(completeLines);
        }

        private void UpdateField(HashSet<int> rows)
        {
            Dictionary<KeyValuePair<int, int>, Color> update = new Dictionary<KeyValuePair<int, int>, Color>();
            foreach (var block in this.board)
            {
                int shift = 0;
                foreach (int i in rows)
                {
                    if (block.Key.Key < i)
                    {
                        shift++;
                    }
                }
                update.Add(new KeyValuePair<int, int>(block.Key.Key + shift, block.Key.Value), block.Value);
            }
            this.board.Clear();
            foreach (var elem in update)
            {
                this.board.Add(elem.Key, elem.Value);
            }
        }

    }
}
