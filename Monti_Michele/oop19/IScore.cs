﻿using System;
using System.Collections.Generic;
using System.Text;

namespace oop19
{
    public interface IScore
    {
        void AddPoints(int combo);

        int Score
        {
            get;
        }
    }
}
