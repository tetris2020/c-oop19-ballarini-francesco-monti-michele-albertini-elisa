using Microsoft.VisualStudio.TestTools.UnitTesting;
using oop19;
using System.Collections.Generic;
using System.Drawing;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        private IScore score = new ScoreImpl();
        private IBoard board = new BoardImpl();
        private HashSet<int> lines = new HashSet<int>();
        private readonly Dictionary<KeyValuePair<int, int>, Color> TRYBOARD1 = new Dictionary<KeyValuePair<int, int>, Color>
        {
            { new KeyValuePair<int, int>(17, 3), Color.White },
            { new KeyValuePair<int, int>(18, 3), Color.White },
            { new KeyValuePair<int, int>(19, 3), Color.White },
            { new KeyValuePair<int, int>(19, 4),Color.White }
        };

        private static readonly Dictionary<KeyValuePair<int, int>, Color> TRYBOARD2 = new Dictionary<KeyValuePair<int, int>, Color>
        {
            { new KeyValuePair<int, int>(2, 3), Color.White },
            { new KeyValuePair<int, int>(3, 3), Color.White },
            { new KeyValuePair<int, int>(4, 3), Color.White },
            { new KeyValuePair<int, int>(4, 4), Color.White }
};

        private static readonly Dictionary<KeyValuePair<int, int>, Color> TRYBOARD3 = new Dictionary<KeyValuePair<int, int>, Color>
        {
            { new KeyValuePair<int, int>(19, 0), Color.White },
            { new KeyValuePair<int, int>(19, 1), Color.White },
            { new KeyValuePair<int, int>(19, 2), Color.White },
            { new KeyValuePair<int, int>(19, 3), Color.White },
            { new KeyValuePair<int, int>(19, 4), Color.White },
            { new KeyValuePair<int, int>(19, 5), Color.White },
            { new KeyValuePair<int, int>(19, 6), Color.White },
            { new KeyValuePair<int, int>(19, 7), Color.White },
            { new KeyValuePair<int, int>(19, 8), Color.White }
        };

        private static readonly HashSet<KeyValuePair<int, int>> TRYCURRENT1 = new HashSet<KeyValuePair<int, int>>
        {
            new KeyValuePair<int, int>(17, 0),
            new KeyValuePair<int, int>(17, 1),
            new KeyValuePair<int, int>(19, 0),
            new KeyValuePair<int, int>(19, 1)
        };

        private static readonly HashSet<KeyValuePair<int, int>> TRYCURRENT2 = new HashSet<KeyValuePair<int, int>>
        {
            new KeyValuePair<int, int>(19, 0),
            new KeyValuePair<int, int>(19, 1),
            new KeyValuePair<int, int>(19, 2),
            new KeyValuePair<int, int>(19, 3),
            new KeyValuePair<int, int>(19, 4),
            new KeyValuePair<int, int>(19, 5),
            new KeyValuePair<int, int>(19, 6),
            new KeyValuePair<int, int>(19, 7),
            new KeyValuePair<int, int>(19, 8),
            new KeyValuePair<int, int>(19, 9)
        };

        private static readonly HashSet<KeyValuePair<int, int>> TRYCURRENT3 = new HashSet<KeyValuePair<int, int>>
        {
            new KeyValuePair<int, int>(18, 0),
            new KeyValuePair<int, int>(18, 1),
            new KeyValuePair<int, int>(18, 2),
            new KeyValuePair<int, int>(18, 3),
            new KeyValuePair<int, int>(18, 4),
            new KeyValuePair<int, int>(18, 5),
            new KeyValuePair<int, int>(18, 6),
            new KeyValuePair<int, int>(18, 7),
            new KeyValuePair<int, int>(18, 8)
        };

        [TestMethod]
        public void BoardTest()
        {
            foreach(var i in TRYCURRENT1)
            {
                this.board.PlacePiece(i, Color.White);
            }
            this.board.Board.Clear();
            foreach(var i in TRYCURRENT3)
            {
                this.board.PlacePiece(i, Color.White);
            }
            foreach(var i in TRYCURRENT2)
            {
                this.board.PlacePiece(i, Color.White);
            }
            this.board.FindRowsCompleted();
            foreach(var i in TRYBOARD3)
            {
                Assert.IsTrue(this.board.Board.ContainsKey(i.Key));
            }
            foreach (var i in TRYCURRENT3)
            {
                this.board.PlacePiece(i, Color.White);
            }
            this.lines.Add(19);
            this.board.CancelLines(this.lines);
            this.lines.Remove(19);
            foreach (var i in TRYBOARD3)
            {
                Assert.IsTrue(this.board.Board.ContainsKey(i.Key));
            }
        }
        [TestMethod]
        public void ScoreTest()
        {
            this.score.AddPoints(1);
            Assert.AreEqual(10, this.score.Score);
            this.score.AddPoints(2);
            Assert.AreEqual(40, this.score.Score);
            this.score.AddPoints(3);
            Assert.AreEqual(100, this.score.Score);
            this.score.AddPoints(4);
            Assert.AreEqual(200, this.score.Score);
        }
    }
}