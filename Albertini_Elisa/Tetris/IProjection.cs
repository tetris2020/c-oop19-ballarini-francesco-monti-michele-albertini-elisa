﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Tetris
{
    /// <summary>
    /// This class has methods for the projection of the piece on the playground.The projection represent the "final" position of the piece orthogonally to the playground.
    /// </summary>
    public interface IProjection
    {
        /// <summary>
        /// Calculates the new Projection.
        /// </summary>
        /// <returns></returns> the coordinates of the projection of the piece on the playground.In
        HashSet<KeyValuePair<int, int>> NewProjection();

        /// <summary>
        /// Calculates the color of the projection.
        /// </summary>
        /// <returns></returns> the color of the projection (the color is the same of the original piece but more transparent) 
        Color GetColor();

        /// <summary>
        /// Sets the projection enable or disable.
        /// </summary>
        /// <param name="isEnabled"></param> isEnabled specifies if the projection must be enabled or disabled.
        void SetEnable(bool isEnabled);

        /// <summary>
        /// Check if projection is enabled.
        /// </summary>
        /// <returns></returns> true if projection is enabled, otherwise it returns false.
        bool IsEnabled();
    }


}
