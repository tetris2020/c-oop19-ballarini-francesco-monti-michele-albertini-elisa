﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Tetris
{
    public class Projection : IProjection
    {
        /// <summary>
        /// Alpha for the projection color.
        /// </summary>
        public const int TRANSPARENCY = 130;
        private readonly GameLogic game;
        private int top;
        private bool isEnabled;

        public Projection(GameLogic game)
        {
            this.game = game;

            isEnabled = true;
        }

        /*
        * Calculates the effective coordinates of the projection.
        */
        private HashSet<KeyValuePair<int, int>> CalculateEffectiveCoordinates(int top)
        {
            // set for new coordinates
            HashSet<KeyValuePair<int, int>> newCoordinates = new HashSet<KeyValuePair<int, int>>();

            // calculate the new coordinates with the current left and the new top
            game.Current.Coordinates.ToList().ForEach(coord => newCoordinates
                    .Add(new KeyValuePair<int, int>(coord.Key + top, coord.Value + game.Current.Left)));

            return newCoordinates;
        }

        public Color GetColor()
        {
            return Color.FromArgb
                (TRANSPARENCY, game.Current.Color.R, game.Current.Color.G, game.Current.Color.B);
        }

        public bool IsEnabled()
        {
            return isEnabled;
        }

        public HashSet<KeyValuePair<int, int>> NewProjection()
        {
            if (isEnabled)
            {
                top = game.Current.Top;

                // calculate the right top
                while (game.IsLegalPosition(CalculateEffectiveCoordinates(top + 1)))
                {
                    top += 1;
                }
                return CalculateEffectiveCoordinates(top);
            }
            else
            {
                return new HashSet<KeyValuePair<int, int>>();
            }
        }

        public void SetEnable(bool isEnabled)
        {
            this.isEnabled = isEnabled;
        }
    }
}
