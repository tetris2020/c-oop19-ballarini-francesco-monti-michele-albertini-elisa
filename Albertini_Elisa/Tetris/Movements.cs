﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tetris
{
    public class Movements : IMovements
    {
        private readonly GameLogic game;

        //These fields are use to revert the fix for the empty columns.
        private int fixEmptyCol;
        private int leftBeforeFix;
        private Piece pieceFixed;
        private bool isFixed;

        public Movements(GameLogic newGame)
        {
            isFixed = false;

            game = newGame;
        }

        public bool DropDown()
        {
            int oldTop = game.Current.Top;

            game.Current.Top = game.Current.Top + 1;

            if (game.IsCurrentLegal())
            {
                return true;
            }
            else
            {
                game.Current.Top = oldTop;
                return false;
            }
        }

        public void MoveLeft()
        {
            Move(-1);
        }

        public void MoveRight()
        {
            Move(1);
        }

        public void RotateClockwise()
        {
            Rotate(true);
        }

        public void RotateCounterclockwise()
        {
            Rotate(false);
        }

        /* The algorithm used to rotate clockwise is: new_x = old_y + center_x -
         * center_y; new_y = center_x + center_y - old_x. The algorithm used to rotate
         * counterclockwise is: new_x = centre_x + centre_y - old_y; new_y = old_x +
         * center_y - centre_x. The method controls also the coordinates (x and y)
         * doesn't became negative, major than the matrix size (in that case they are
         * fixed) and it also cancels the empty columns. If the piece is straight on the
         * left or right wall, on the bottom or straight to dead pieces fixes are
         * applied to allow the rotation. The method sets the new coordinates only if
         * they are legal. I use a bool, isClockwise, to choose witch kind of
         * rotation is required.
         */
        private void Rotate(bool isClockwise)
        {
            // in case movement is not legal I save all the field of the piece before
            // rotate it
            int oldLeft = game.Current.Left;
            int oldTop = game.Current.Top;

            KeyValuePair<int, int> oldCenter = game.Current.Center;

            HashSet<KeyValuePair<int, int>> oldCoordinates = game.Current.Coordinates;

            // set for new coordinates
            HashSet<KeyValuePair<int, int>> newCoordinates = new HashSet<KeyValuePair<int, int>>();

            // rotate
            if (isClockwise)
            {
                game.Current.Coordinates.ToList().ForEach(coord => newCoordinates.Add(new KeyValuePair<int, int>(
                        coord.Value + game.Current.Center.Key - game.Current.Center.Value,
                        game.Current.Center.Key + game.Current.Center.Value - coord.Key)));
            }
            else
            {
                game.Current.Coordinates.ToList().ForEach(coord => newCoordinates.Add(new KeyValuePair<int, int>(
                    game.Current.Center.Key + game.Current.Center.Value - coord.Value,
                    coord.Key + game.Current.Center.Value - game.Current.Center.Key)));
            }

            // sets new coordinates
            game.Current.Coordinates = newCoordinates;

            // fixs coordinates
            FixCoordinatesIfNecessary();


            // controls the coordinates
            if (!game.IsCurrentLegal())
            {
                FixIfCurrentIsNotLegal(oldLeft);
            }

            //control and revert
            if (!game.IsCurrentLegal())
            {
                game.Current.Left = oldLeft;
                game.Current.Top = oldTop;
                game.Current.Center = oldCenter;
                game.Current.Coordinates = oldCoordinates;
            }
        }

        /*
         * Calculates the max of X
         */
        private int MaxX()
        {
            return game.Current.Coordinates.Select(x => x.Key).Max();
        }

        /*
         * Calculates the max of Y
         */
        private int MaxY()
        {
            return game.Current.Coordinates.Select(x => x.Value).Max();
        }

        /*
         * Calculates the min of X
         */
        private int MinX()
        {
            return game.Current.Coordinates.Select(x => x.Key).Min();
        }

        /*
         * Calculates the min of Y
         */
        private int MinY()
        {

            return game.Current.Coordinates.Select(x => x.Value).Min();
        }

        /*
         * Calculates the Y size of the piece
         */
        private int YPieceSize()
        {
            return MaxY() - MinY() + 1;
        }

        /*
         * Calculates the X size of the piece
         */
        private int XPieceSize()
        {
            return MaxX() - MinX() + 1;
        }

        /*
         * Fix coordinates under zero. It also fixes the left and the center of the
         * piece if it is necessary.
         */
        private void FixUnderZero()
        {
            // set for new coordinates
            HashSet<KeyValuePair<int, int>> newCoordinates = new HashSet<KeyValuePair<int, int>>();

            // calculate the min of x and y to check if they are negative
            int minX = MinX();
            int minY = MinY();

            int fixX;
            int fixY;

            // check the negative x
            if (minX < 0)
            {
                if (game.Current.Top < 0)
                {
                    game.Current.Top = 0;
                }

                fixX = Math.Abs(minX);
            }
            else
            {
                fixX = 0;
            }

            // check the negative y
            if (minY < 0)
            {
                if (game.Current.Left < 0)
                {
                    game.Current.Left = 0;
                }

                fixY = Math.Abs(minY);
            }
            else
            {
                fixY = 0;
            }

            // at least one of the two must be different from 0
            if (fixX != 0 || fixY != 0)
            {
                // all the coordinates, even the center, are translated to change all the
                // negative coordinates in positive
                // fix center
                game.Current.Center = new KeyValuePair<int, int>(game.Current.Center.Key + fixX,
                        game.Current.Center.Value + fixY);

                // fix coordinates
                game.Current.Coordinates.ToList().ForEach(coord => newCoordinates.Add(
                    new KeyValuePair<int, int>(coord.Key + fixX, coord.Value + Math.Abs(fixY))));

                // set new coordinates
                game.Current.Coordinates = newCoordinates;
            }
        }

        /*
         * Translates to left the piece in its "matrix" if there are some empty columns
         * on the left.
         */
        private void FixEmptyColumns()
        {
            // set for new coordinates
            HashSet<KeyValuePair<int, int>> newCoordinates = new HashSet<KeyValuePair<int, int>>();

            // calculate the min of y to check the empty columns
            int minY = MinY();

            // in this case there are some empty column I fix coordinates and center to
            // eliminate them, and fix the left
            // increasing it by the quantity of empty rows to not "see" the fix
            if (minY > 0)
            {
                // this function save all the useful field to revert the fix applied on the

                // left
                UsedFixEmptyCol(minY);

                // fix center
                game.Current.Center = new KeyValuePair<int, int>(game.Current.Center.Key,
                        game.Current.Center.Value - minY);

                // fix left
                game.Current.Left = game.Current.Left + minY;

                // fix coordinates
                game.Current.Coordinates.ToList().ForEach(coord => newCoordinates.Add
                (new KeyValuePair<int, int>(coord.Key, coord.Value - minY)));

                // set new coordinates
                game.Current.Coordinates = newCoordinates;
            }
            else
            {
                isFixed = false;
            }
        }

        /*
         * Saves all the parameters useful to revert the fix. 
         * The parameter used is minY
         * (the corrective factor used in the fix).
         */
        private void UsedFixEmptyCol(int minY)
        {
            isFixed = true;
            leftBeforeFix = game.Current.Left;
            fixEmptyCol = minY;
            pieceFixed = game.Current;
        }

        /*
         * Reverts the fix for the empty columns so the piece doesn't translate when it
         * rotates. This fix is applied just in case the piece is the same.
         */
        private void RevertFixEmptyCol()
        {
            // if fix was applied on the piece itself
            if (isFixed && pieceFixed.Equals(game.Current))
            {
                int fix;
                // in case the left changed
                if (leftBeforeFix - game.Current.Left > 0)
                {
                    fix = fixEmptyCol - (leftBeforeFix - game.Current.Left);
                }
                else
                {
                    fix = fixEmptyCol;
                }

                game.Current.Left = game.Current.Left - Math.Abs(fix);
            }

            isFixed = false;
        }

        /*
         * Fixes coordinates out of the "matrix" of the piece.
         */
        private void FixCoordinatesOverSize()
        {
            // set for new coordinates
            HashSet<KeyValuePair<int, int>> newCoordinates = new HashSet<KeyValuePair<int, int>>();

            // to check the over size coordinates I calculate the max of x and y
            int maxX = MaxX();
            int maxY = MaxY();

            int fixX;
            int fixY;

            // in this case X are over size
            if (maxX > (Piece.MATRIXDIMENSION - 1))
            {
                fixX = maxX - (Piece.MATRIXDIMENSION - 1);
            }
            else
            {
                fixX = 0;
            }

            // in this case Y are over size
            if (maxY > (Piece.MATRIXDIMENSION - 1))
            {
                fixY = maxY - (Piece.MATRIXDIMENSION - 1);
            }
            else
            {
                fixY = 0;
            }

            // at least one of the two must be different from 0
            if (fixX != 0 || fixY != 0)
            {
                // in case they are over size I reduce the coordinates and the center by the
                // difference between the max and the matrix dimension

                // fix center
                game.Current.Center = new KeyValuePair<int, int>(game.Current.Center.Key - fixX,
                        game.Current.Center.Value - fixY);

                // fix coordinates
                game.Current.Coordinates.ToList().ForEach(coord => newCoordinates.Add
                (new KeyValuePair<int, int>(coord.Key - fixX, coord.Value - fixY)));

                // set new coordinates
                game.Current.Coordinates = newCoordinates;
            }
        }

        /*
         * Fixes all problems the piece could have rotating, such as coordinates become
         * negatives, the piece has empty columns on the left or the piece come out from
         * its own "matrix".
         */
        private void FixCoordinatesIfNecessary()
        {
            // revert the fix for the empty column possibly applied on the previous rotation
            RevertFixEmptyCol();

            // fix negative coordinates
            FixUnderZero();

            // fix over size coordinates
            FixCoordinatesOverSize();

            // fix empty columns
            FixEmptyColumns();
        }

        /*
         * Tries every fix methods necessary if the final coordinates are not legal.
         */
        private void FixIfCurrentIsNotLegal(int oldLeft)
        {
            int replaceTop = game.Current.Top;

            // management of horizontal case
            FixRotationHorizontalCase();

            // check the new position is legal
            if (!game.IsCurrentLegal())
            {
                // restore the old top
                //game.Current.Top = replaceTop;
                game.Current.Top = replaceTop;

                // management of vertical case
                FixRotationVerticalCase(oldLeft);
            }
        }

        /*
         * Fix the rotation if the isLegal method says the piece can't move, but he actually
         * can. In fact, in particular case, the piece has just not enough space to
         * rotate on the bottom. So this method changes the top of the piece so it can
         * move.
         */
        private void FixRotationHorizontalCase()
        {
            // if there are some dead piece on the bottom I calculate their length
            if (game.Board.Count() != 0)
            {
                int length = game.Board.Keys.Where(x => x.Value == game.Current.Left).Select(x => x.Key).Min();

                // the new top is the length of the dead piece - the piece size
                /// game.Current.Top = length - XPieceSize();
                game.Current.Top = length - XPieceSize();
            }
            else
            {
                // if there aren't dead piece the new top is the board length - the piece size
                ////game.Current.Top = GameLogic.COLLENGTH - XPieceSize();
                game.Current.Top = GameLogic.COLLENGTH - XPieceSize();
            }
        }

        /*
         * Fix the rotation if the isLegal method says it can't move, but he actually
         * can. In fact, in particular case, the piece has just not enough space to
         * rotate on the right or on the left. So this method translate the left of the
         * piece so it can move. This method test two different case (to the left and to
         * the right).
         */
        private void FixRotationVerticalCase(int oldLeft)
        {
            int left = game.Current.Left;
            // LEFT CASE

            // this is the case when the piece is not adjacent to the left but it can't
            // rotate

            // the first case is solved increasing by one the left
            game.Current.Left = left + 1;

            // check the new position is legal
            if (!game.IsCurrentLegal())
            {
                // this is the case when the piece is adjacent on the left
                // I set the left equals to the old one
                game.Current.Left = oldLeft;
            }
            if (!game.IsCurrentLegal())
            {
                // RIGHT CASE

                // this is the case when the piece is not adjacent to the right but it can't
                // rotate

                // the first case is solved reducing by one the left
                game.Current.Left = left - 1;

                // check the new position is legal
                if (!game.IsCurrentLegal())
                {
                    // this is the case when the piece is adjacent on the right
                    // difference between the old and the new pos translated
                    game.Current.Left = left - (left + YPieceSize() - oldLeft - XPieceSize());
                }
            }
        }

        /*
         * Shifts the piece of a factor "direction". 
         * The parameter used is direction:
         * the factor of translation.
         */
        private void Move(int direction)
        {
            int oldLeft = game.Current.Left;

            game.Current.Left = game.Current.Left + direction;

            if (!game.IsCurrentLegal())
            {
                game.Current.Left = oldLeft;
            }
        }
    }
}
