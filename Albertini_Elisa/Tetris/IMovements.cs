﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// This class implements all the methods to move safety all the kind of pieces
/// (the classic ones and the custom ones). In particular, the piece rotates
/// around its center in two different ways: clockwise and counterclockwise.It 
/// also translates in two direction: to the left and to the right and drops
/// down.
/// </summary>
namespace Tetris
{
    public interface IMovements
    {
        /// </summary>
        ///Rotates the piece clockwise.
        /// </summary>
        void RotateClockwise();

        /// </summary>
        /// Rotates the piece counterclockwise.
        /// </summary>
        void RotateCounterclockwise();

        /// </summary>
        /// Shifts left the piece.
        /// </summary>
        void MoveLeft();

        /// </summary>
        /// Shifts right the piece.
        /// </summary>
        void MoveRight();

        /// </summary>
        /// Drop down the piece.
        ///</summary>
        ///<returns></returns>  true, if drop down is successful otherwise it returns false.
        bool DropDown();

    }
}
