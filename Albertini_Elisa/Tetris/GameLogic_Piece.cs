﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

/// <summary>
/// This two classes are not a complete trasnposition of the original ones (they where made by one of my project
/// mates in the java project).
/// They are used to let the class Movements and Projection and their tests work properly 
/// (there are just few methods used by my classes and some necessary fields).
/// </summary>
namespace Tetris
{
    public class GameLogic
    {
        public const int ROWLENGTH = 10;
        public const int COLLENGTH = 20;
        private Piece current;
        private Dictionary<KeyValuePair<int, int>, System.Windows.Media.Color> board;

        /// <summary>
        /// Gets and sets current piece.
        /// </summary>
        public Piece Current
        {
            get { return current; }
            set { current = value; }
        }

        /// <summary>
        /// Gets the board.
        /// </summary>
        public Dictionary<KeyValuePair<int, int>, Color> Board
        {
            get { return board; }
        }

        public GameLogic(Piece newPiece)
        {
            board = new Dictionary<KeyValuePair<int, int>, Color>();
            current = newPiece;
        }

        /// <summary>
        /// Controls if the position of a piece is allowed or not.
        /// </summary>
        /// <param name="coords"></param> Coordinates of the position of the piece.
        /// <returns></returns> True if the position is allowed atherwise it return false.
        public bool IsLegalPosition(HashSet<KeyValuePair<int, int>> coords)
        {
            foreach (KeyValuePair<int, int> i in coords)
            {
                //no game over controls
                if (board.ContainsKey(i) || i.Key >= COLLENGTH || i.Value < 0
                        || i.Value >= ROWLENGTH)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Controls if the position of the current piece is allowed or not.
        /// </summary>
        /// <returns></returns>
        public bool IsCurrentLegal()
        {
            return IsLegalPosition(current.GetPosition());
        }
    }

    public class Piece
    {
        public const int MATRIXDIMENSION = 4;
        private const int LEFT = 3;

        private HashSet<KeyValuePair<int, int>> coordinates;
        private KeyValuePair<int, int> center;
        private int left;
        private int top;
        private Color color;

        /// <summary>
        /// Gets and sets left.
        /// </summary>
        public int Left
        {
            get { return left; }
            set { left = value; }
        }

        /// <summary>
        /// Gets and sets top.
        /// </summary>
        public int Top
        {
            get { return top; }
            set { top = value; }
        }

        /// <summary>
        /// Gets Color.
        /// </summary>
        public Color Color
        {
            get { return color; }
        }

        /// <summary>
        /// Gets and sets coordinates.
        /// </summary>
        public HashSet<KeyValuePair<int, int>> Coordinates
        {
            get { return coordinates; }
            set { coordinates = value; }
        }

        /// <summary>
        /// Gets and sets center
        /// </summary>
        public KeyValuePair<int, int> Center
        {
            get { return center; }
            set { center = value; }
        }

        public Piece(HashSet<KeyValuePair<int, int>> newCoordinates, KeyValuePair<int, int> newCenter)
        {
            coordinates = newCoordinates;

            center = newCenter;

            top = 0;

            left = LEFT;

            //thie field will be not used, so I initialize it with a random color
            color = new Color();
        }

        /// <summary>
        /// Gets the coordinates of the position of the piece.
        /// </summary>
        /// <returns></returns>
        public HashSet<KeyValuePair<int, int>> GetPosition()
        {
            HashSet<KeyValuePair<int, int>> newCoords = new HashSet<KeyValuePair<int, int>>();

            foreach (KeyValuePair<int, int> i in coordinates)
            {
                newCoords.Add(new KeyValuePair<int, int>(i.Key + top, i.Value + left));
            }
            return newCoords;
        }
    }
}
