﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tetris;

/// <summary>
/// Test for class Movements and Projection
/// </summary>
namespace Test
{
    [TestClass]
    public class Test
    {
        //MOVEMENTS TESTS
        private const int SIZE_1 = 2;
        private const int SIZE_2 = 3;
        private const int EXPECTED_LEFT = 4;
        private const int EXPECTED_TOP = 0;

        // sets with coordinates of the Rotated pieces
        // L piece
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_L = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(0, 2)
        };

        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_1_L = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(2, 0),
            new KeyValuePair<int, int>(2, 1)
        };

        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_2_L = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(2, 0)
        };

        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_3_L = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(2, 1),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(0, 1)
        };

        // I piece
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_I = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(0, 2),
            new KeyValuePair<int, int>(0, 3)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_1_I = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(2, 0),
            new KeyValuePair<int, int>(3, 0)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_2_I = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(1, 3)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_3_I = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(2, 0),
            new KeyValuePair<int, int>(2, 1),
            new KeyValuePair<int, int>(2, 2),
            new KeyValuePair<int, int>(2, 3)
        };
        // J piece
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_J = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 2)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_1_J = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(2, 0)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_2_J = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(2, 2)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_3_J = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(2, 0),
            new KeyValuePair<int, int>(2, 1)
        };
        // S piece
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_S = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(0, 2),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_1_S = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(2, 1)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_2_S = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(2, 0),
            new KeyValuePair<int, int>(2, 1)
        };
        // Z piece
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_Z = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(1, 1)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_1_Z = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(2, 0)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_2_Z = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(2, 1),
            new KeyValuePair<int, int>(2, 2)
        };
        // T piece
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_T = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(0, 1)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_1_T = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(2, 0)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_2_T = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(2, 1)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_3_T = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(2, 1)
        };
        // CUSTOM piece (created by me)
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_CUSTOM = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(2, 0),
            new KeyValuePair<int, int>(2, 1),
            new KeyValuePair<int, int>(2, 2),
            new KeyValuePair<int, int>(3, 1),
            new KeyValuePair<int, int>(3, 2),
            new KeyValuePair<int, int>(3, 3)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_1_CUSTOM = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(0, 2),
            new KeyValuePair<int, int>(0, 3),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(2, 0),
            new KeyValuePair<int, int>(2, 1),
            new KeyValuePair<int, int>(3, 0)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_2_CUSTOM = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(0, 2),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(1, 3),
            new KeyValuePair<int, int>(2, 2),
            new KeyValuePair<int, int>(2, 3),
            new KeyValuePair<int, int>(3, 3)
        };
        private static readonly HashSet<KeyValuePair<int, int>> COORDINATES_3_CUSTOM = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 3),
            new KeyValuePair<int, int>(1, 2),
            new KeyValuePair<int, int>(1, 3),
            new KeyValuePair<int, int>(2, 1),
            new KeyValuePair<int, int>(2, 2),
            new KeyValuePair<int, int>(2, 3),
            new KeyValuePair<int, int>(3, 0),
            new KeyValuePair<int, int>(3, 1),
            new KeyValuePair<int, int>(3, 2)
        };
        private static readonly GameLogic game = new GameLogic
            (new Piece(COORDINATES_J, new KeyValuePair<int, int>(0, 1)));
        private readonly IMovements mov = new Movements(game);

        /// <summary>
        /// Tests the movement to the right. I test just the left of a random piece.
        /// </summary>
        [TestMethod]
        public void TestMoveRight()
        {
            game.Current.Left = 3;

            mov.MoveRight();
            Assert.AreEqual(EXPECTED_LEFT, game.Current.Left);
            game.Current.Left = GameLogic.ROWLENGTH;

            mov.MoveRight();
            Assert.AreEqual(GameLogic.ROWLENGTH, game.Current.Left);
        }

        /// <summary>
        /// Tests the movement to the left. I test just the left of a random piece.
        /// </summary>
        [TestMethod]
        public void TestMoveLeft()
        {
            game.Current.Left = EXPECTED_LEFT;

            mov.MoveLeft();
            Assert.AreEqual(EXPECTED_LEFT - 1, game.Current.Left);

            game.Current.Left = 0;

            mov.MoveLeft();
            Assert.AreEqual(0, game.Current.Left);
        }

        /// <summary>
        /// Tests the fix for rotation on the left wall (it is shifted to the right).
        /// </summary>
        [TestMethod]
        public void TestFixRotationLeft()
        {
            // example with piece J
            game.Current = new Piece(COORDINATES_J, new KeyValuePair<int, int>(1, 1))
            {
                // this is the right position
                Left = 0
            };
            Assert.AreEqual(0, game.Current.Left);

            mov.RotateClockwise();

            Assert.AreEqual(1, game.Current.Left);
            game.Current.Left = 0;

            // now the piece is adjacent to the left wall
            mov.RotateClockwise();
            Assert.AreEqual(0, game.Current.Left);
        }

        /// <summary>
        /// Test the fix for rotation on the right wall (it is shifted to theleft).
        /// </summary>
        [TestMethod]
        public void TestFixRotationRight()
        {
            // example with piece J
            game.Current = new Piece(COORDINATES_J, new KeyValuePair<int, int>(1, 1))
            {
                // right position
                Left = GameLogic.ROWLENGTH - SIZE_2
            };
            Assert.AreEqual(GameLogic.ROWLENGTH - SIZE_2, game.Current.Left);

            mov.RotateCounterclockwise();
            Assert.AreEqual(GameLogic.ROWLENGTH - SIZE_2, game.Current.Left);

            game.Current.Left = game.Current.Left + 1;

            // now the piece is adjacent to the left wall
            mov.RotateCounterclockwise();
            Assert.AreEqual(GameLogic.ROWLENGTH - SIZE_2, game.Current.Left);
        }

        /// <summary>
        /// Test the fix for rotation on the bottom.
        /// </summary>
        [TestMethod]
        public void TestFixRotationBottom()
        {
            // example with piece J
            game.Current = new Piece(COORDINATES_J, new KeyValuePair<int, int>(1, 1))
            {
                Top = GameLogic.COLLENGTH - SIZE_1
            };
            Assert.AreEqual(GameLogic.COLLENGTH - SIZE_1, game.Current.Top);

            mov.RotateClockwise();
            Assert.AreEqual(GameLogic.COLLENGTH - SIZE_2, game.Current.Top);
        }

        /// <summary>
        /// Tests the drop down. I test just the top of a random piece.
        /// </summary>
        [TestMethod]
        public void TestDropDown()
        {
            mov.DropDown();
            Assert.AreEqual(EXPECTED_TOP + 1, game.Current.Top);
        }

        /// <summary>
        /// Tests the clockwise rotation of piece L. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateClockwiseL()
        {
            // L piece
            game.Current = new Piece(COORDINATES_L, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(COORDINATES_L.SetEquals(game.Current.Coordinates));

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_1_L.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_2_L.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_3_L.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_L.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the clockwise rotation of piece I. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateClockwiseI()
        {
            game.Current = new Piece(COORDINATES_I, new KeyValuePair<int, int>(0, 1));
            Assert.IsTrue(COORDINATES_I.SetEquals(game.Current.Coordinates));

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_1_I.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_2_I.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 2), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_1_I.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(2, 0), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_3_I.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(2, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the clockwise rotation of piece S. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateClockwiseS()
        {
            game.Current = new Piece(COORDINATES_S, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(COORDINATES_S.SetEquals(game.Current.Coordinates));

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_1_S.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_2_S.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_1_S.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_S.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the clockwise rotation of piece J. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateClockwiseJ()
        {
            game.Current = new Piece(COORDINATES_J, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(COORDINATES_J.SetEquals(game.Current.Coordinates));

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_1_J.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_2_J.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_3_J.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_J.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the clockwise rotation of piece Z. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateClockwiseZ()
        {
            game.Current = new Piece(COORDINATES_Z, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(COORDINATES_Z.SetEquals(game.Current.Coordinates));

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_1_Z.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_2_Z.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_1_Z.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_Z.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the clockwise rotation of piece T. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateClockwiseT()
        {
            game.Current = new Piece(COORDINATES_T, new KeyValuePair<int, int>(1, 1));
            Assert.AreEqual(COORDINATES_T, game.Current.Coordinates);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_1_T.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_2_T.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_3_T.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_T.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the clockwise rotation of piece CUSTOM. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateClockwiseCUSTOM()
        {
            game.Current = new Piece(COORDINATES_CUSTOM, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(COORDINATES_CUSTOM.SetEquals(game.Current.Coordinates));

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_1_CUSTOM.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 2), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_2_CUSTOM.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(2, 2), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_3_CUSTOM.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(2, 1), game.Current.Center);

            mov.RotateClockwise();
            Assert.IsTrue(COORDINATES_CUSTOM.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the counterclockwise rotation of piece L. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateCounterclockwiseL()
        {
            game.Current = new Piece(COORDINATES_L, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(COORDINATES_L.SetEquals(game.Current.Coordinates));

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_3_L.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_2_L.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_1_L.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_L.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the counterclockwise rotation of piece I. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateCounterclockwiseI()
        {
            game.Current = new Piece(COORDINATES_I, new KeyValuePair<int, int>(0, 1));
            Assert.IsTrue(COORDINATES_I.SetEquals(game.Current.Coordinates));

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_1_I.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(2, 0), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_3_I.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(2, 2), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_1_I.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_2_I.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the counterclockwise rotation of piece S. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateCounterclockwiseS()
        {
            game.Current = new Piece(COORDINATES_S, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(COORDINATES_S.SetEquals(game.Current.Coordinates));

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_1_S.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_2_S.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_1_S.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_S.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the counterclockwise rotation of piece J. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateCounterclockwiseJ()
        {
            game.Current = new Piece(COORDINATES_J, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(COORDINATES_J.SetEquals(game.Current.Coordinates));

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_3_J.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_2_J.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_1_J.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_J.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the counterclockwise rotation of piece Z. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateCounterclockwiseZ()
        {
            game.Current = new Piece(COORDINATES_Z, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(COORDINATES_Z.SetEquals(game.Current.Coordinates));

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_1_Z.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_2_Z.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_1_Z.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_Z.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the counterclockwise rotation of piece T. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateCounterclockwiseT()
        {
            game.Current = new Piece(COORDINATES_T, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(COORDINATES_T.SetEquals(game.Current.Coordinates));

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_3_T.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_2_T.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_1_T.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 0), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_T.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        /// <summary>
        /// Tests the counterclockwise rotation of CUSTOM piece. Checks also the center.
        /// </summary>
        [TestMethod]
        public void TestRotateCounterclockwiseCUSTOM()
        {
            game.Current = new Piece(COORDINATES_CUSTOM, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(COORDINATES_CUSTOM.SetEquals(game.Current.Coordinates));

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_3_CUSTOM.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(2, 1), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_2_CUSTOM.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(2, 2), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_1_CUSTOM.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 2), game.Current.Center);

            mov.RotateCounterclockwise();
            Assert.IsTrue(COORDINATES_CUSTOM.SetEquals(game.Current.Coordinates));
            Assert.AreEqual(new KeyValuePair<int, int>(1, 1), game.Current.Center);
        }

        //PROJECTION TESTS

        private const int TOP = 18;
        private const int LEFT = 3;
        private static readonly HashSet<KeyValuePair<int, int>> PROJECTION_COORDINATES = new HashSet<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(1 + TOP, 0 + LEFT),
            new KeyValuePair<int, int>(1 + TOP, 1 + LEFT),
            new KeyValuePair<int, int>(1 + TOP, 2 + LEFT),
            new KeyValuePair<int, int>(0 + TOP, 2 + LEFT)
        };

        private static readonly HashSet<KeyValuePair<int, int>> DISABLED_COORDINATES = new HashSet<KeyValuePair<int, int>>();
        private readonly IProjection proj = new Projection(game);

        /// <summary>
        /// Tests the projection in case it is enabled or disabled.
        /// </summary>
        [TestMethod]
        public void TestNewProjection()
        {
            game.Current = new Piece(COORDINATES_L, new KeyValuePair<int, int>(1, 1));
            Assert.IsTrue(proj.IsEnabled());
            Assert.IsTrue(proj.NewProjection().SetEquals(PROJECTION_COORDINATES));

            proj.SetEnable(false);
            Assert.IsFalse(proj.IsEnabled());
            Assert.IsTrue(proj.NewProjection().SetEquals(DISABLED_COORDINATES));
        }
    }
}