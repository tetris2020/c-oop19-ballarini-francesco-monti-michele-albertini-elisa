﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task;
using Optional;
using System.Drawing;
using System.Collections.Generic;
using System;
using Optional.Unsafe;

namespace TestTask
{
    [TestClass]
    public class Test
    {
        readonly Color tempColor = Color.FromName("Beige");
        readonly List<KeyValuePair<int, int>> tempList = new List<KeyValuePair<int, int>>();
        readonly KeyValuePair<int, int> tempCenter = new KeyValuePair<int, int>(0, 0);
        readonly IPlayer tempPlayer = new Player("mario", "mariopassword", "italia", 21);


        [TestMethod]
        public void Custom()
        {
            tempList.Add(tempCenter);

            ICustom customTemp = new Custom(tempColor, tempList, tempCenter);

            Assert.AreEqual(customTemp.Coords.Count, 1);

            Assert.AreEqual(customTemp.Center, tempCenter);

            customTemp.Color = Color.FromName("Blue").ToArgb();

            Assert.AreNotEqual(customTemp.Color, tempColor);
            Assert.IsFalse(customTemp.Coords.Contains(new KeyValuePair<int, int>(2, 2)));

        }
        [TestMethod]
        public void Player()
        {
            Assert.IsFalse(tempPlayer.CustomPiece.HasValue);
            Assert.IsNotNull(tempPlayer.BestScore);
            Assert.AreEqual(tempPlayer.BestScore, 0);

            tempPlayer.AddCustomPiece(new Custom(Color.Aqua, tempList, tempCenter));
            tempPlayer.AddCustomPiece(new Custom(Color.BlanchedAlmond, tempList, tempCenter));
            tempPlayer.AddCustomPiece(new Custom(Color.DeepPink, tempList, tempCenter));
            tempPlayer.BestScore = 50;

            Assert.AreEqual(tempPlayer.CustomPiece.ValueOr(new List<ICustom>()).Count, 3);
            Assert.AreEqual(tempPlayer.BestScore, 50);
        }
        [TestMethod]
        public void ControllerCustomAndManager()
        {
            IControllerManager manager = new ControllerManager();
            IControllerCustomPiece controller = new ControllerCustomPiece(manager);

            Assert.IsFalse(manager.Player.HasValue);
            Assert.IsFalse(manager.RtCustom.HasValue);
            Assert.IsFalse(manager.TempCustom.HasValue);

            Assert.IsNotNull(controller.Squares);
            Assert.AreEqual(controller.Squares.Count, 0);

            Assert.ThrowsException<Exception>(controller.SaveAttempt);

            controller.Hit(new KeyValuePair<int, int>(0,0));

            Assert.AreEqual(controller.Squares.Count, 1);
            Assert.ThrowsException<Exception>(controller.SaveAttempt);

            controller.Hit(new KeyValuePair<int, int>(1, 1));

            Assert.AreEqual(controller.Squares.Count, 2);
            Assert.ThrowsException<Exception>(controller.SaveAttempt);

            controller.Hit(new KeyValuePair<int, int>(0, 1));
            Assert.AreEqual(controller.Squares.Count, 3);

            controller.Hit(new KeyValuePair<int, int>(1, 1));
            Assert.AreEqual(controller.Squares.Count, 2);

            controller.SaveAttempt();
            Assert.AreEqual(controller.Squares.Count, 0);
            Assert.IsTrue(manager.RtCustom.HasValue);

            controller.Hit(new KeyValuePair<int, int>(0, 0));
            controller.Hit(new KeyValuePair<int, int>(0, 1));
            Assert.ThrowsException<Exception>(controller.SaveAttempt);

            controller.Hit(new KeyValuePair<int, int>(0, 2));
            controller.SaveAttempt();
            Assert.AreEqual(manager.RtCustom.ValueOrFailure().Count,2);
            Assert.AreEqual(controller.Squares.Count, 0);

            manager.Player = Option.Some(tempPlayer);
            Assert.IsTrue(manager.Player.HasValue);
            Assert.IsFalse(manager.Player.ValueOrFailure().CustomPiece.HasValue);

            controller.Hit(new KeyValuePair<int, int>(0, 0));
            controller.Hit(new KeyValuePair<int, int>(0, 1));
            controller.SaveAttempt();
            Assert.AreEqual(manager.RtCustom.ValueOrFailure().Count, 2);
            Assert.AreEqual(manager.Player.ValueOrFailure().CustomPiece.ValueOrFailure().Count, 1);
            Assert.AreEqual(controller.Squares.Count, 0);

            controller.Hit(new KeyValuePair<int, int>(0, 0));
            controller.Hit(new KeyValuePair<int, int>(0, 1));
            Assert.ThrowsException<Exception>(controller.SaveAttempt);

            manager.Player = Option.None<IPlayer>();
            Assert.IsFalse(manager.Player.HasValue);
            
            controller.ClearData();
            Assert.AreEqual(controller.Squares.Count, 0);

            controller.Hit(new KeyValuePair<int, int>(0, 0));
            controller.Hit(new KeyValuePair<int, int>(0, 1));
            controller.Hit(new KeyValuePair<int, int>(0, 2));
            controller.Hit(new KeyValuePair<int, int>(0, 3));
            controller.SaveAttempt();
            Assert.AreEqual(manager.RtCustom.ValueOrFailure().Count, 3);
        }

    }
}

