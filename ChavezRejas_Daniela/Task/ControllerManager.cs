﻿using Optional;
using Optional.Unsafe;
using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public class ControllerManager : IControllerManager
    {
        public ControllerManager()
        {
            Player = Option.None<IPlayer>();
            TempCustom = Option.None<List<ICustom>>();
            RtCustom = Option.None<List<ICustom>>();
        }

        public EnumFactory Controller { get; set; }
        public IView View { get; set; }
        public Levels Speed { get; set; }
        public Option<IPlayer> Player { get; set; }
        public Option<List<ICustom>> RtCustom { get; set; }
        public Option<List<ICustom>> TempCustom{ get; set; }

        public void InitRtCustomList()
        {
            RtCustom = Option.Some(new List<ICustom>());
        }
    }
}
