﻿using System.Collections.Generic;

namespace Task
{
    public interface ICustom
    {
    int Color
        {
            get;
            set;
        }

    List<KeyValuePair<int, int>> Coords
        {
            get;
            set;
        }

    KeyValuePair<int, int> Center
        {
            get;
            set;
        }
    }
}
