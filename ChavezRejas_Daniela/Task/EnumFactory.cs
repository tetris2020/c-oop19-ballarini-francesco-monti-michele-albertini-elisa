﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public enum EnumFactory
    {
        Menu,
        Settings,
        Custom,
        Game,
        Login,
        Profile
    }
}
