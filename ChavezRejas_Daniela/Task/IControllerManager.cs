﻿using System.Collections.Generic;
using Optional;

namespace Task
{
    public interface IControllerManager
    {
        EnumFactory Controller
        {
            set;
        }

        IView View
        {
            set;
        }

        Levels Speed
        {
            get;
            set;
        }

        Option<IPlayer> Player
        {
            get;
            set;
        }
        Option<List<ICustom>> TempCustom
        {
            get;
            set;
        }

        Option<List<ICustom>> RtCustom
        {
            get;
            set;
        }

        void InitRtCustomList();
    }
}
