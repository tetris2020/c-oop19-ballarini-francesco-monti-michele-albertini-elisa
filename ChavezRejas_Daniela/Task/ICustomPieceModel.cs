﻿using Optional;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Task
{
    interface ICustomPieceModel
    {  
        void Add(KeyValuePair<int, int> hit);
        void Delete(KeyValuePair<int, int> deleteSquare);
        Color CurrentColor
        {
            get;
            set;
        }

        ISet<KeyValuePair<int, int>> HitSquares
        {
            get;
        }

        void Refresh();

        bool IsUnique(Option<List<ICustom>> checkList);

        void SaveRtPiece( Option<List<ICustom>> rtList);

        void SavePlayerPiece( IPlayer player);
    }
}
