﻿using System.Collections.Generic;
using System.Drawing;

namespace Task
{
    public interface IControllerCustomPiece
    {
       
        Color CurrentColor
        {
            set;
        }

        void Hit(KeyValuePair<int, int> hit);

        void SaveAttempt();

        void ClearData();
                
        ISet<KeyValuePair<int, int>> Squares
        {
            get;
        }

    }
}
