﻿using System.Collections.Generic;
using System.Drawing;

namespace Task
{
    public class Custom : ICustom

    {
        public Custom(Color newColor, List<KeyValuePair<int, int>> newCoords, KeyValuePair<int, int> newCenter)
        {
            Color = newColor.ToArgb();
            Coords = newCoords;
            Center = newCenter;
        }

        public List<KeyValuePair<int, int>> Coords { get; set; }
        public KeyValuePair<int, int> Center { get; set; }
        public int Color { get; set; }
    }
}
