﻿using Optional;
using Optional.Unsafe;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Task
{
    class CustomPieceModel : ICustomPieceModel
    {
        private static readonly Color defaultColor = Color.Pink;

        public CustomPieceModel()
        {
            CurrentColor = defaultColor;
            HitSquares = new HashSet<KeyValuePair<int, int>>();
        }

        public Color CurrentColor { get; set; }

        public ISet<KeyValuePair<int, int>> HitSquares { get; }

        public void Add(KeyValuePair<int, int> hit)
        {
            HitSquares.Add(hit);
        }

        public void Delete(KeyValuePair<int, int> deleteSquare)
        {
            HitSquares.Remove(deleteSquare);
        }

        public bool IsUnique(Option<List<ICustom>> checkList)
        {
            if (!checkList.HasValue)
            {
                return true;
            }
            ICustom temp = GetCustom();

            return !checkList.ValueOrFailure().FindAll(b => b.Coords.Count == temp.Coords.Count)
                                                .Any(b => b.Coords.All(c => temp.Coords.Contains(c)));
        }

        private Custom GetCustom()
        {

            ISet<KeyValuePair<int, int>> fixedCoordinates = new HashSet<KeyValuePair<int,int>>();

            int minX = HitSquares.Min(x => x.Key);
            int minY = HitSquares.Min(x => x.Value);

            if (minX > 0 || minY > 0)
            {
                List<KeyValuePair< int, int >> tempList = new List<KeyValuePair<int, int>>();
                foreach(var e in HitSquares)
                {
                    tempList.Add(new KeyValuePair<int, int>(e.Key - minX, e.Value - minY));
                }
                fixedCoordinates = tempList.ToHashSet();

            }
            else
            {
                fixedCoordinates = HitSquares;
            }
            return new Custom(CurrentColor,fixedCoordinates.ToList(),GetCenter(fixedCoordinates));

        }

        private KeyValuePair<int, int> GetCenter(ISet<KeyValuePair<int, int>> fixedCoords)
        {
            int xCenter;
            int yCenter;
            List<int> listX;
            List<int> listY;

            listX = fixedCoords.Select(p => p.Key).ToList();
            listX.Sort();
            xCenter = listX.ElementAt(0) + listX.ElementAt(listX.Count -1) / 2;
            listY = fixedCoords.Select(p => p.Value).ToList();
            listX.Sort();
            yCenter = listX.ElementAt(0) + listX.ElementAt(listX.Count - 1) / 2;

            return new KeyValuePair<int, int>(xCenter, yCenter);
        }

        public void Refresh()
        {
            CurrentColor = defaultColor;
            HitSquares.Clear();
        }

        public void SavePlayerPiece( IPlayer player)
        {
            List<ICustom> tempPieces = new List<ICustom>();

            if (player.CustomPiece.HasValue)
            {
                tempPieces.AddRange(player.CustomPiece.ValueOrFailure());
            }
            tempPieces.Add(GetCustom());
            player.CustomPiece = Option.Some(tempPieces);
        }

        public void SaveRtPiece( Option<List<ICustom>> rtList)
        {
            rtList.ValueOrFailure().Add(GetCustom());
        }
    }
}
