﻿using Optional;
using System.Collections.Generic;

namespace Task
{
    public class Player : IPlayer
    {
        private static readonly int startScore = 0;

        public Player(string name, string password, string country, int age)
        {
            Name = name;
            Password = password;
            Country = country;
            Age = age;
            CustomPiece = Option.None<List<ICustom>>();
            BestScore = startScore;
            }

        public Option<List<ICustom>> CustomPiece { get; set; }
        public int BestScore { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public int Age { get; set; }
        public string Password { get; set; }

        public void AddCustomPiece(Custom customPiece)
        {
            List<ICustom> temp = CustomPiece.ValueOr(new List<ICustom>());
            temp.Add(customPiece);
            CustomPiece = Option.Some(temp);
        }

    }

}
