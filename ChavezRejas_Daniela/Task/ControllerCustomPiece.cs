﻿using Optional;
using Optional.Unsafe;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Task
{
    public class ControllerCustomPiece : IControllerCustomPiece
    {
        private readonly IControllerManager manager;
        private readonly ICustomPieceModel model;

        public ControllerCustomPiece(IControllerManager mainManager)
        {
            manager = mainManager;
            model = new CustomPieceModel();
        }
        public Color CurrentColor { 
            get
            {
               return model.CurrentColor;
            }
            set
            {
                if (!model.CurrentColor.Equals(value))
                {
                    model.CurrentColor = value;
                }
            }
            }
        public void Hit (KeyValuePair<int, int> hit)
        {
            if (model.HitSquares.Contains(hit))
            {
                model.Delete(hit);
            }
            else
            {
                model.Add(hit);
            }
        }
        public ISet<KeyValuePair<int, int>> Squares => model.HitSquares;

        public void ClearData()
        {
            model.Refresh();   
        }

        private bool IsTypeCorrect()
        {

            bool correctType = true;

            if (Squares.Count <= 1)
            {
                correctType = false;
            }
            else
            {
                ISet<KeyValuePair<int, int>> wrongs = new HashSet<KeyValuePair<int, int>>();
                foreach (var p in Squares)
                {
                    if (!(Squares.Contains(new KeyValuePair<int, int>(p.Key - 1, p.Value))
                            || Squares.Contains(new KeyValuePair<int, int>(p.Key + 1, p.Value))
                            || Squares.Contains(new KeyValuePair<int, int>(p.Key, p.Value - 1))
                            || Squares.Contains(new KeyValuePair<int, int>(p.Key, p.Value + 1))))
                    {
                        wrongs.Add(p);
                    }
                };

                if (wrongs.Count > 0)
                {
                    correctType = false;
                }
            }

            return correctType;
        }

        public void SaveAttempt()
        {
            if (IsTypeCorrect())
            {
                if (manager.Player.HasValue)
                {
                    Option<List<ICustom>> temp = manager.Player.ValueOrFailure().CustomPiece;
                    
                    if (model.IsUnique(temp))
                    {
                        model.SavePlayerPiece(manager.Player.ValueOrFailure());
                        model.Refresh();
                    }
                    else
                    {
                        throw new Exception();
                        //this.customView.alreayOnCustomPieceList();
                    }
                }
                else
                {
                    if (!manager.RtCustom.HasValue)
                    {
                        manager.InitRtCustomList();
                    }
                    if (model.IsUnique(manager.RtCustom))
                    {
                        model.SaveRtPiece(manager.RtCustom);
                        model.Refresh();
                    }
                    else
                    {
                        throw new Exception();
                        //this.customView.alreayOnCustomPieceList();
                    }
                }
            }
            else
            {
                throw new Exception();
                //this.customView.incorrectTypePiece();
            }
        }

        
    }
}
