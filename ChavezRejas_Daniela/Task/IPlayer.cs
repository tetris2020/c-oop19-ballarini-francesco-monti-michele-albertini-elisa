﻿using System;
using System.Collections.Generic;
using Optional;
using System.Text;

namespace Task
{
    public interface IPlayer
    {
        void AddCustomPiece(Custom customPiece);
      
        Option<List<ICustom>> CustomPiece
        {
            get;
            set;
        }

        int BestScore
        {
            get;
            set;
        }

        string Name
        {
            get;
            set;
        }

        string Country
        {
            get;
            set;
        }

        int Age
        {
            get;
            set;
        }

        string Password
        {
            get;
            set;
        }

    }
}
